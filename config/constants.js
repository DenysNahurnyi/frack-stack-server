function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value,
        enumerable: true,
        writable: false,
        configurable: false
    });
}

exports.responseStatus = {};
exports.secret = () => {return "DannySecret"}

define(exports.responseStatus, 'CLIENT_ERROR', 400);
define(exports.responseStatus, 'SERVER_ERROR', 500);
define(exports.responseStatus, 'DUPLICATE_ENTRY', 400);
define(exports.responseStatus, 'INVALID_ACCESS_TOKEN', 401);
define(exports.responseStatus, 'PARAMETER_MISSING', 300);
define(exports.responseStatus, 'NOT_REGISTERED', 2);
define(exports.responseStatus, 'SUCCESS', 200);
define(exports.responseStatus, 'ERROR_IN_EXECUTION', 400);

exports.responseMessage = {};
define(exports.responseMessage, 'SAND_SITE_NOT_AVAILABLE_AT_THIS_TIME', 'SAND SITE NOT AVAILABLE AT THIS TIME');
define(exports.responseMessage, 'NO_JOB_COMPLETED', 'NO JOB COMPLETED AT THIS TIME');

define(exports.responseMessage, 'NO_JOB', 'NO JOB IS ASSIGNED CURRENTLY');
define(exports.responseMessage, 'UNFINISHED_JOB_REMAINING', 'Please finish the job before completing it');
define(exports.responseMessage, 'PARAMETER_MISSING', 'Some value is missing');
define(exports.responseMessage, 'ERROR_IN_EXECUTION', 'Something went wrong');
define(exports.responseMessage, 'EMAIL_NOT_EXISTS', 'This email is not registered');
define(exports.responseMessage, 'INCORRECT_CREDENTIALS', 'Your email or password is incorrect');
define(exports.responseMessage, 'SUCCESS', 'Success');
define(exports.responseMessage, 'EMAIL_EXIST', 'This email address already exists');
define(exports.responseMessage, 'TRUCK_NUMBER_EXIST', 'This Truck number already exists');
define(exports.responseMessage, 'TRAILER_NUMBER_EXIST', 'This Trailer number already exists');
define(exports.responseMessage, 'INVALID_ACCESS_TOKEN', 'Please login again , invalid access');
define(exports.responseMessage, 'DISTICT_EXIST', 'This distict already exists');
define(exports.responseMessage, 'LOCATION_EXIST', 'This location(latitude and longitude) already exists');
define(exports.responseMessage, 'SAND_TYPE_EXIST', 'This type of sand already exists');
define(exports.responseMessage, 'SAND_TYPE_EXIST_FOR_SAND_SITE', 'This type of sand already exists for this sand site');
define(exports.responseMessage, 'LOG_OUT', 'You are successfully logged out');
define(exports.responseMessage, 'NO_JOB_ASSIGNED', ' CURRENTLY YOU HAVE NO JOB ASSIGNED');
define(exports.responseMessage, 'DRIVER_VEHICLE_DETAILS', 'DRIVER VEHICLE DETAILS UPDATED');
define(exports.responseMessage, 'PROFILE_SUCCESSFULLY_UPDATED', 'DRIVER PROFILE SUCCESSFULLY UPDATED');
define(exports.responseMessage, 'JOB_DETAILS_ACCEPTED', 'DRIVER ACCEPTED JOB');
define(exports.responseMessage, 'REJECT_JOB', 'DRIVER REJECTED JOB');
define(exports.responseMessage, 'LOAD_ARRIVAL_TIME_UPDATED', 'LOAD ARRIVAL TIME UPDATED');
define(exports.responseMessage, 'LOAD_DEPARTURE_TIME_UPDATED', 'LOAD DEPARTURE TIME UPDATED');
define(exports.responseMessage, 'WELL_ARRIVAL_TIME_UPDATED', 'WELL ARRIVAL TIME UPDATED');
define(exports.responseMessage, 'WELL_DEPARTURE_TIME_UPDATED', 'WELL DEPARTURE TIME UPDATED');
define(exports.responseMessage, 'SAND_COORDINATOR_CREATED', 'SAND COORDINATOR SUCCESSFULLY CREATED');
define(exports.responseMessage, 'EMAIL_SENT', 'EMAIL SENT');
define(exports.responseMessage, 'CANT_LOGOUT', 'SORRY YOU NEED TO FINISH YOUR JOB BEFORE LOGOUT');
define(exports.responseMessage, 'NO_SAND_COORDINATOR', 'SORRY THIS SAND COORDINATOR IS NOT AVAILABLE');
define(exports.responseMessage, 'CERTIFICATE_SUCCESSFULLY_UPLOADED', 'CERTIFICATE_SUCCESSFULLY_UPLOADED');
define(exports.responseMessage, 'CHANGE_PASSWORD', 'PASSWORD SUCCESSFULLY UPDATED');
define(exports.responseMessage, 'JOB_MANAGER_CREATED', 'JOB MANAGER SUCCESSFULLY CREATED');
define(exports.responseMessage, 'INCORRECT_PASSWORD', 'CURRENT password is incorrect');
define(exports.responseMessage, 'PROFILE_UPDATED', 'PROFILE SUCCESSFULLY UPDATED');
define(exports.responseMessage, 'PO_ALREADY_EXIST', 'THIS PURCHASE ORDER NUMBER ALREADY EXISTS');
define(exports.responseMessage, 'NOT_A_VALID_ENTRY', 'INSUFFICIENT AMOUNT TO AUTO POPULATE THE LOADS');
define(exports.responseMessage, 'VERIFIED_SUCCESSFULLY', 'YOU HAVE SUCCESSFULLY VERIFIED THE DRIVER');
define(exports.responseMessage, 'EMAIL_SEND', 'Thank You We Will Get Back To You Soon');

exports.jobOrderStatus = {};

define(exports.jobOrderStatus, 'Ideal', 0);
define(exports.jobOrderStatus, 'DriverAssignedButJobNotAccepted', 1);
define(exports.jobOrderStatus, 'JobAccepted', 2);
define(exports.jobOrderStatus, 'DepartLoadingSite', 3);
define(exports.jobOrderStatus, 'DepartWellSite', 4);
define(exports.jobOrderStatus, 'EnteringLoadingSite', 5);
define(exports.jobOrderStatus, 'EnteringWellSite', 6);

exports.settingType = {};

define(exports.settingType, 'MaxDistance', 1);
define(exports.settingType, 'TimeBeforeLoadIsQueued', 2);

