const bluebird = require('bluebird');
const config = require('config');
const mysql = require('promise-mysql');

let pool;

exports.initialize = () => {
    try {
        pool = mysql.createPool({
            host: config.get('databaseSettings.host'),
            database: config.get('databaseSettings.database'),
            user: config.get('databaseSettings.user'),
            password: config.get('databaseSettings.password'),
            connectionLimit: 100
        });
    } catch(err) {

    }
};

exports.connect = async () => {
    try {
        return pool.getConnection().catch(()=>{});
    } catch(err) {
        
    }
};

exports.release = connection => {
    try{
        pool.releaseConnection(connection);        
    } catch(err) {

    }
};

exports.check = async (delay, notify = true) => {
    if (notify) {
        log.info('Database', 'Checking database');
    }

    let connection;

    try {
        connection = await exports.connect();
        exports.release(connection);

        log.info('Database', 'Database ready');

        return true;
    } catch (err) {
        await bluebird.delay(delay);
        await exports.check(delay, false);
    }
};
