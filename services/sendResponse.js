let sender = {};
sender.send = function(res, status, data) {
    res.json({
        status,
        data
    })
}

module.exports = sender;