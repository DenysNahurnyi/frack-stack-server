const express = require('express');
const jwt = require('jsonwebtoken');

const jwtVerify = require('express-jwt');

const constant = require('../config/constants');
const database = require('../infrastructure/database');
const sender = require('../services/sendResponse');
const middleWare = require('../middleWares/checkToken');


var router = express.Router();

router.post('/test',
    middleWare.checkToken,
    // jwtVerify({
    //     secret: constant.secret()
    // }),
    async (req, res) =>  {
    try{

        const connection = await database.connect();
        let response = await getDataByToken(connection);
        
        let data = {
            response: response[0],
            user: req.user
        }

        sender.send(res, constant.responseStatus.SUCCESS, data);

    } catch (err) {
        sender.send(res, constant.responseMessage.SERVER_ERROR, response[0]);
    } finally {
        database.release(connection);
    }
    
});

async function getDataByToken(connection) {
    const job_manager = await connection.query(`
                SELECT email 
                FROM job_manager 
                WHERE id=1
                LIMIT 1
                `, []);
    return job_manager;
}


module.exports = router;
